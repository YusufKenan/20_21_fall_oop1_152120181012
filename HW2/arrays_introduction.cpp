#include <iostream>
#include <cstdio>
using namespace std;
int main() {
    int num;
    cin >> num;             
    int* A = new int[num];             /// Kullanicidan alinan sayiya gore Dynamic array tanimlandi
    for (int i = 0; i < num; i++) 
        cin >> A[i];                   /// Degerler alindi.
    for (int i = num - 1; i >= 0; i--) /// Array ters olacak sekilde for loop acildi.
        cout << A[i] << " ";
    system("pause");
}
