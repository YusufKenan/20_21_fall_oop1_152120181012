#include <iostream>
#include <sstream>
using namespace std;
class Student {
private:

    int age;
    int standard;
    string first_name;
    string last_name;

public:
    void set_age(int a) {        
        age = a;           /// Age degeri kullanicidan alindi.
    }
    void set_standard(unsigned b) { 
        standard = b;     /// Standart degeri kullan�cidan alindi.
    }
    void set_first_name(string c) { 
        first_name = c;   /// First name deferi kullanicidan alindi.
    }
    void set_last_name(string d) {
        last_name = d;    /// Last name degeri kullanicidan alindi.
    }
    int get_age() {
        return age;       
    }
    int get_standard() {
        return standard;
    }
    string get_first_name() {
        return first_name;
    }
    string get_last_name() {
        return last_name;
    }
    string to_string()
    {
        stringstream x;
        x << age << "," << first_name << "," << last_name << "," << standard;   /// �stenilen sekilde sirasiyla yazdirildi.
        return x.str();      /// x de�eri return edildi.
    }

};
int main() {
    int age, standard;
    string first_name, last_name;

    cin >> age >> first_name >> last_name >> standard;

    Student st;
    st.set_age(age);
    st.set_standard(standard);
    st.set_first_name(first_name);
    st.set_last_name(last_name);

    cout << st.get_age() << "\n";
    cout << st.get_last_name() << ", " << st.get_first_name() << "\n";
    cout << st.get_standard() << "\n";
    cout << "\n";
    cout << st.to_string();
    cout << endl;
    system("pause");
}