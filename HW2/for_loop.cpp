#include <iostream>
#include <cstdio>
using namespace std;
int main() {
    int sayi1, sayi2;
    cin >> sayi1 >> sayi2;  /// Kullanicidan 2 adet sayi alindi.
    for (int n = sayi1; n <= sayi2; n++)  /// Alinan sayi araligina gore for loop olusturuldu.
    {
        /// Girilen sayilara gore degerleri yazdirildi.
        if (n == 1)
            cout << "one" << endl;
        else if (n == 2)
            cout << "two" << endl;
        else if (n == 3)
            cout << "three" << endl;
        else if (n == 4)
            cout << "four" << endl;
        else if (n == 5)
            cout << "five" << endl;
        else if (n == 6)
            cout << "six" << endl;
        else if (n == 7)
            cout << "seven" << endl;
        else if (n == 8)
            cout << "eight" << endl;
        else if (n == 9)
            cout << "nine" << endl;
        else if (n % 2 == 0)
            cout << "even" << endl;
        else
            cout << "odd" << endl;
    }
    system("pause");
}