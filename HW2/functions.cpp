#include <iostream>
#include <cstdio>
using namespace std;
int max_of_four(int, int, int, int);
int main() {
    int a, b, c, d;
    scanf_s("%d %d %d %d", &a, &b, &c, &d); /// Kullanicidan sayilar alindi.
    int ans = max_of_four(a, b, c, d);      /// Fonksiyon cagirildi ve return degeri ans degerine esitlendi.
    printf("%d", ans);

    system("pause");
}
int max_of_four(int a, int b, int c, int d)
{
    if (a > b && a > c && a > d)  /// a degerinin en buyuk olma durumu
        return a;
    else if (b > a && b > c && b > d) /// b degerinin en buyuk olma durumu
        return b;
    else if (c > a && c > b && c > d) /// c degerinin en buyuk olma durumu
        return c;
    else                              /// Diger hicbir durumun karsilanmadigi zaman d en buyuk olma durumu.
        return d;
}