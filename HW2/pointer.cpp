#include <iostream>
using namespace std;
void update(int* a, int* b) 
{
    int c = *a; /// Gecici c degeri olusturulup a pointerinin degeri atandi.
    *a = *a + *b; /// Yeni a pointeri 2 pointerin toplamina esitlendi.
    *b = abs(c - *b); /// Eski a pointeri ve b pointeri birbirinden cikartilip mutlak degeri b pointerine esitlendi.
}
int main()
{
	int a,b;
    int* pa = &a, * pb = &b;

    scanf_s("%d %d", &a, &b);
    update(pa, pb);
    printf("%d\n%d", a, b);

    system("pause");
}