#include <iostream>
using namespace std;

int main() {
    string str;
    cin >> str; /// Kullanicidan string degerleri olarak alindi.
    for (int i = 0; i < str.size(); i++) /// string boyutu kadar for loop donduruldu.
    {
        if (str[i] != ',') /// Virgule esit olmadigi durumlarda sayi yazdirildi.
            cout << str[i];
        else               /// Diger durumda ise alt satira gecildi.
            cout << endl;
    }
    cout << endl;
    system("pause");
}