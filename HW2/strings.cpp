#include <iostream>
#include <string>
using namespace std;
int main()
{
	string a, b; /// 2 adet string tanimlandi.
	char temp;
	cin >> a >> b; /// Kullanicidan 2 adet string degeri istendi.
	cout << a.length() << " " << b.length() << endl; /// Girilen degerler sonucundaki uzunluklari yazdirildi.
	cout << a + b << endl;    /// 2 stringin beraber yazildigi durum.
	temp = a[0];              /// a stringinin ilk degeri temp degiskenine atandi.
	a[0] = b[0];              /// a stringinin ilk degeri b stringinin ilk degeri ile degistirildi.
	b[0] = temp;              /// b stringinin ilk degeri temp degeri yani a stringinin onceki ilk degeri ile degistirildi.
	cout << a << " " << b << endl;   /// Yeni durum yazdirildi.
	system("pause");
}