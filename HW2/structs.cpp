#include <iostream>
#include <string>
using namespace std;
struct Student { /// Student Struct'i tanimlandi.

    /// Istenilen degiskenler tanimlandi.
    int age;         
    string first_name;
    string last_name;
    int standard;
};
int main() {
    Student st;

    cin >> st.age >> st.first_name >> st.last_name >> st.standard; /// Structda tanimlandi degerler kullanicidan icerisine atandi.
    cout << st.age << " " << st.first_name << " " << st.last_name << " " << st.standard; /// Bu degerler tek tek yazdirildi.
    cout << endl << endl;
    system("pause");
}