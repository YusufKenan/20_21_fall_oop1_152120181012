#include <iostream>
#include <fstream>
#include <string>
#include <istream>
#include <cmath>
#define boyut 100  ///Dizinin max boyutu tanimlandi.
using namespace std;
int sayi;
int sum(int[]); ///Sum islemini yapacak fonksiyon tanimlandi.
int product(int[]); ///Product islemini yapacak fonksiyon tanimlandi.
int smallest(int[]); /// En k�c�k sayiyi bulacak fonksiyon tanimlandi.
int main()
{
	int x, summ = 0, productt = 1, smallestt, sayac = 0, temp, A[boyut]; /// Gerekli tanimlamalar yapildi.
	float avg;
	fstream datafile;
	datafile.open("input.txt");
	if (datafile.is_open()) /// Dosyanin acillma durumu
	{
		datafile >> sayi; /// Dosyanin ilk satirindaki sayi alindi.

		if (sayi <= 0) /// Dosyadaki ilk satirdaki sayinin negatif olma durumda hata olusmasi.
		{
			cout << "Hata! Sayi pozitif degil.";
			exit(0);
		}
		datafile.get(); /// Dosyanin ilk satirindaki sayidan sonra new line karakteri icin koyuldu.
		for (int i = 0; i < boyut; i++)
		{
			temp = NULL;
			datafile >> temp; /// Dosyadan alinan sayilar gecici bir degiskene atandi.
			if (temp == NULL || temp == '\0' || temp == ' ') /// Dosyadaki sayi olmadigi durumda dongu kirildi.
			{
				break;
			}
			else
			{
				A[i] = temp;
				sayac++; /// Dosyadaki ilk satir haric sayilarin kac adet oldugu tespit edildi.
			}
		}
		if (sayac<sayi || sayac>sayi)
		{
			cout << "Hata! Sayilar uyusmuyor.";
			exit(0);
		}
		datafile.close(); /// Dosyada baska bir durum kalmadigi icin dosya kapatildi.
		summ = sum(A);
		productt = product(A);
		smallestt = smallest(A); /// Tanimlanan fonksiyonlar cagirildi.
		avg = (float)summ / sayi;
		cout << "Sum is " << summ << endl;
		cout << "Product is " << productt << endl;
		cout << "Average is " << avg << endl;
		cout << "Smallest is " << smallestt << endl;
	}
	else
		cout << "Dosya acilamadi!" << endl; /// Dosyanin acilamama durumu

	cout << endl;
	system("pause");
}
int sum(int Arr[])
{
	int sum = 0;
	for (int i = 0; i < sayi; i++)
		sum += Arr[i]; /// Arraydeki bulunan sayilar toplandi.
	return sum;
}
int product(int Arr[])
{
	int product = 1;
	for (int i = 0; i < sayi; i++)
		product *= Arr[i]; /// Arraydeki bulunan sayilar carpildi.
	return product;
}
int smallest(int Arr[])
{
	int small = Arr[0];
	for (int i = 0; i < sayi; i++)
		if (Arr[i] < small) /// Arraydeki bulunan en k�c�k sayi tespit edildi.
			small = Arr[i];

	return small;
}
